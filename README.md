Updated version is on [github](https://github.com/ohadeytan/TechnionVideosDownload)

# Goal #
This bash script let you download videos of lectures & tutorials from the old technion server (http://video.technion.ac.il)

# Dependencies #
You must install msdl (http://msdl.sourceforge.net)

# Username and Password #
If you don't want to enter your username & password every time you can edit the script and enter them in lines 5-6 

# Responsibility #
Any use of this script is at the user own responsibility and should be according to the Israeli law and the Technion rules

# Usage #
* Argument 1: link to the course page
* Argument 2: optional - from video number
* Argument 3: optional - to video numer